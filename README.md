# Welcome to Wellfound's microblog assessment!

## Goal
Your task is to update the home page of this Flask app to display all of the rows in the blog posts table instead of just the static single row.

Clone/fork this repository and make a commit with this change. After you've made your changes, please take a screenshot of the updated home page and include it in the README of your public repository. Finally, email the link to your public repository to clayton.bridge@wellfound.co, and please include your full name in the email!

<img width="1181" alt="Screenshot 2023-03-20 at 3 46 36 PM" src="https://user-images.githubusercontent.com/3745832/226482900-007cf156-7191-489e-9e92-7880aeff2f57.png">

<BR><BR>

## Contributions
Feel free to clone this repo locally and use your own editor of choice.

If you would rather use the Gitpod development environment for this app:

- Change the dropdown that says "Web IDE" to "Gitpod" (if it already says "Gitpod" skip this step)
- Click the button that says "Gitpod"
