#!/bin/bash
sudo yum update
sudo yum install pip nginx git -y
cd /home/ec2-user
    # files need to be set as ec2-user ownership to avoid permission errors
sudo -u ec2-user git clone https://gitlab.com/chriskelly1/microblog.git
git clone https://gitlab.com/chriskelly1/microblog.git
cd microblog
sudo pip install -r requirements.txt
# Configure Nginx
sudo sh -c 'cat > /etc/nginx/conf.d/flaskapp.conf <<EOF
server {
    listen 80;
    server_name _;

    location / {
        proxy_pass http://127.0.0.1:8000;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
    }
}
EOF'
# Configure microblog service
sudo sh -c 'cat > /etc/systemd/system/microblog.service <<EOF
[Unit]
Description=Gunicorn instance for microblog
After=network.target
[Service]
User=ec2-user
Group=ec2-user
WorkingDirectory=/home/ec2-user/microblog
ExecStart=/usr/local/bin/gunicorn -b localhost:8000 microblog:app
Restart=always
Environment=DATABASE_URL=${database_url}
[Install]
WantedBy=multi-user.target
EOF'
# Start application
sudo systemctl enable nginx
sudo systemctl start nginx
sudo systemctl start microblog