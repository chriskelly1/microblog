terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "us-west-2"
}

# Create security group for EC2 instance
resource "aws_security_group" "blog_server" {
  name_prefix = "blog-server-sg"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
}

# Expects 'TF_VAR_database_url' to be a environment variable with the PostgreSQL engine URL
# or run 'TF_VAR_database_url=<PostgreSQL engine URL> terraform apply'
variable "database_url" {
  type = string
}

data "template_file" "main_script" {
  template = file("${path.module}/main.sh")
  vars = {
    database_url = var.database_url
  }
}

resource "aws_instance" "blog_server" {
  ami           = "ami-0efa651876de2a5ce"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.blog_server.id]
  key_name = "key-pair-1"
  user_data     = data.template_file.main_script.rendered # initial script to run
  tags = {
    Name = "microblog"
  }
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 Instance"
  value       = aws_instance.blog_server.public_ip
}